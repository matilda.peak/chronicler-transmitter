#!/usr/bin/env python

# Setup module for the Python-based Chronicler transmitter library.
#
# August 2019

from setuptools import setup
import os


# Pull in the essential run-time requirements
with open('requirements.txt') as r_file:
    requirements = r_file.read().splitlines()


def get_long_description():
    return open('README.rst').read()


setup(

    name='matildapeak-chronicler-transmitter',
    version=os.environ.get('CI_COMMIT_TAG', '2019.1'),
    author='Alan Christie',
    author_email='alan.christie@matildapeak.com',
    url='https://gitlab.com/matilda.peak/chronicler-transmitter',
    license='MIT',
    description='Utilities for Chronicler event delivery',
    long_description=get_long_description(),
    keywords='protobuf,chronicler',
    platforms=['any'],

    # Our modules to package
    package_dir={'': '.'},
    packages=['matildapeak'],

    # Supported Python versions
    # 3.7 or better
    python_requires='>=3.7, <4',

    # Project classification:
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Other Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Operating System :: POSIX :: Linux',
    ],

    install_requires=requirements,

    zip_safe=False,

)
