---
Title:      A Chronicler Transmitter Library
Author:     Alan Christie
Date:       25 August 2019
Copyright:  Matilda Peak. All rights reserved.
---

[![pipeline status](https://gitlab.com/matilda.peak/chronicler-transmitter/badges/master/pipeline.svg)](https://gitlab.com/matilda.peak/chronicler-transmitter/commits/master)

![PyPI - License](https://img.shields.io/pypi/l/matildapeak-chronicler-transmitter)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/matildapeak-chronicler-transmitter)
![PyPI](https://img.shields.io/pypi/v/matildapeak-chronicler-transmitter)
![PyPI - Status](https://img.shields.io/pypi/status/matildapeak-chronicler-transmitter)
![PyPI - Wheel](https://img.shields.io/pypi/wheel/matildapeak-chronicler-transmitter)

# The Chronicler transmitter
A simple Python class to handle event transmission to Chronicler.
Utilities used by [Garten] and other Chronicler delivery modules
with the Python module available on [PyPi].

---

[garten]: https://gitlab.com/matilda.peak/garten
[pypi]: https://pypi.org/project/matildapeak-chronicler-transmitter
